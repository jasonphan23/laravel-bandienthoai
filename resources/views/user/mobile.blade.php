@extends('layout.master')

@section('mobile-filter')


<div class="col-md-12">

  </div>
  <div class="select-boxes-section">
    <div class="header-right">
<div class="form-group col-md-3">
  <label for="select_brand">Hãng:</label>
  <select class="form-control" id="select_brand">
        <option value="" selected>Tất cả</option>
    @foreach($brands as $brand)
        <option value="{{$brand->id}}" >{{$brand->name}}</option>
    @endforeach
  </select>
</div>
<div class="form-group col-md-3">
  <label for="select_cost">Giá:</label>
  <select class="form-control" id="select_cost">
    <option value="all" selected>Tất cả</option>
    <option value="under1mil" >Dưới 1 triệu</option>
    <option value="1to3mil" >1-3 Triệu</option>
    <option value="3to7mil" >3-7 Triệu</option>
    <option value="7to10mil" >7-10 Triệu</option>
    <option value="above10mil" >Trên 10 Triệu</option>
  </select>
</div>
</div>
</div>
</div>
@endsection


@section('active-nav')
<li><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown active"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li ><a href="/gioithieu">Về chúng tôi</a></li>
<li><a href="/lienhe">Liên hệ</a></li>

@endsection


@section('content')


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="col-md-12" id="mobile-container">
          @include('layout.mobile-container-partial')
            </div>
            </div>


        <div class="row">
            <div class="col-md-12">
                <div class="product-pagination text-center">

                </div>
            </div>
        </div>
  </div>



  <script>


  $( document ).ready(function() {
    $("#select_brand").change(function(event) {
       event.preventDefault();
      var brandVal= $("#select_brand").val();
      var costVal = $("#select_cost").val();
      $.ajax({
        url: '/advanced-search',
        type: 'GET',
        dataType : 'html',
        data: {brand: brandVal ,cost: costVal},
        success:function(data) {
          $("#mobile-container").html(data);
        }
    })
  })
  $("#select_cost").change(function(event) {
     event.preventDefault();
    var brandVal= $("#select_brand").val();
    var costVal = $("#select_cost").val();
    $.ajax({
      url: '/advanced-search',
      type: 'GET',
      dataType : 'html',
      data: {brand: brandVal ,cost: costVal},
      success:function(data) {
        $("#mobile-container").html(data);
      }
  })
})
});
  </script>

@endsection
