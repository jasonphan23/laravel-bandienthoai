@extends('layout.master')
@section('active-nav')
<li class=""><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li ><a href="/gioithieu">Về chúng tôi</a></li>
<li><a href="/lienhe">Liên hệ</a></li>

@endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#info" aria-controls="home" role="tab" data-toggle="tab">Thông tin cá nhân</a></li>
      <li role="presentation"><a href="#historybill" aria-controls="profile" role="tab" data-toggle="tab">Lịch sử mua hàng</a></li>

    </ul>
    </div>
    <div class="col-sm-9">
      <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
        <div class="woocommerce">

          <form method = "post" action="{{ route('postsuathongtinuser') }}">
            @if(session('status'))
                            <p class="alert alert-success">{{session('status')}}</p>
                    @endif
                    @if(session('fail'))
                            <p class="alert alert-danger">{{session('fail')}}</p>
                    @endif

                                      @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h2>THÔNG TIN CƠ BẢN </h2>
            <p> (*) : bắt buộc để thuận tiện cho việc liên lạc khi giao hàng
            <div class="form-group">
              <label for="real_name">Họ Tên</label> (*)
              <input type="text" class="form-control" name="real_name" value="{{ $user->real_name }}" required>
            </div>
            <div class="form-group">
              <label for="email">Email:</label> (*)
                                            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>
            </div>
            <div class="form-group">
              <label for="address">Địa chỉ:</label> (*)
              <input type="text" class="form-control" name="address" value="{{ $user->address }}" required>
            </div>
            <div class="form-group">
              <label for="phone">Điện thoại:</label> (*)
              <input type="text"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'  class="form-control" name="phone" value="{{ $user->phone }}" required>
            </div>
              <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
              </div>
        </form>

            <h2>THAY ĐỔI MẬT KHẨU </h2>
            <form class="form-horizontal form-material" method="post" action="{{ route('postsuapassword') }}">
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label class="col-md-12">Password mới</label>
                                        <div class="col-md-12">
                                            <input type="password" value="" class="form-control form-control-line" name="password" required> </div>
                                    </div>
                                      <div class="form-group">
                                          <label for="example-email" class="col-md-12">Nhập lại Password mới</label>
                                          <div class="col-md-12">
                                              <input type="password" value="" class="form-control form-control-line" name="password_confirmation" required > </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-12">Nhập password cũ</label>
                                          <div class="col-md-12">
                                              <input type="password" value="" class="form-control form-control-line" name="old_password" required> </div>
                                      </div>
                                        <div class="form-group">
                                      <div class="col-sm-12">
                                              <input type="submit" class="btn btn-success" value="Cập Nhật">
                                      </div>
                                    </div>
                                  </form>
                       </div>
      </div>

      <div role="tabpanel" class="tab-pane" id="historybill">
      <h2>LỊCH SỬ MUA HÀNG</h2>
<table class="table">
 <thead>
   <tr>
     <th>Ngày mua</th>
     <th>Tổng tiền</th>
     <th>Trạng thái</th>
     <th>Loại Thanh Toán</th>
     <th>Chi Tiết</th>
   </tr>
 </thead>
 <tbody>
   @foreach($lst_donhang as $dh)

   <tr>
     <td>{{ $dh->created_at }}</td>
     <td>{{ number_format($dh->summary,0) }} VNĐ</td>
     @if($dh->status == 0)
           <td> <div class="alert alert-info">
Chưa xử lý
</div>
 </td>
     @elseif($dh->status == 1)
          <td>
        <div class="alert alert-warning">
        Đang xử lý
        </div>
          </td>
     @elseif($dh->status == 2)
     <td>
   <div class="alert alert-success">
     Đã Hoàn Thành
   </div>
     </td>
     @else
     <td>
   <div class="alert alert-danger">
     Đã Hủy
   </div>
     </td>
     @endif
     @if($dh->loai_thanh_toan==0)
         <td> COD </td>
     @else
         <td> PAYPAL </td>
     @endif
     <td><button type="button" class="btn btn-info btn-lg xemchitiet" name="{{$dh->id}}" data-toggle="modal" data-target="#myModal" id="{{$dh->id}}">Xem Chi Tiết</button></td>
   </tr>

@endforeach

 </tbody>
</table>

   <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" id="cthd-container">
      @isset($order_details)
      @include('layout.modalctdh-partial');
      @endisset
    </div>
  </div>
</div>
    </div>
    </div>
  </div>
</div>
</div>
<script>
$( document ).ready(function() {
  $(".xemchitiet").click(function(event) {
     event.preventDefault();
     var id =  $(this).attr('id');
     console.log(id);
    $.ajax({
      url: '/xemchitietdonhang',
      type: 'GET',
      dataType : 'html',
      data: {order_id: id },
      success:function(data) {
        $("#cthd-container").html(data);
        console.log('du');
      }
  })
})
});
</script>

@endsection
