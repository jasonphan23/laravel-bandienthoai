@extends('layout.master')
@section('active-nav')
<li class=""><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li class="active"><a href="/gioithieu">Về chúng tôi</a></li>
<li><a href="/lienhe">Liên hệ</a></li>

@endsection
@section('content')


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
              <h2>SHOP ĐIỆN THOẠI FREEDOM MOBILE<h2>
                <p>Freedom Mobile chúng tôi chuyên về lĩnh vực buôn bán các mặt hàng điện thoại của các thương hiệu trên Thế Giới.
                   Freedom Mobile luôn coi trọng chữ Tín như chính danh dự của mình và luôn kinh doanh trên nền tảng trung thực.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
