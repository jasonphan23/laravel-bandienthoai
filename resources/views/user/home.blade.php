@extends('layout.master')
@section('promo')
  @include('layout.promo')
@endsection
@section('active-nav')
<li class="active"><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li><a href="/gioithieu">Về chúng tôi</a></li>
<li><a href="/lienhe">Liên hệ</a></li>

@endsection
@section('content')

<div class="maincontent-area">
  <div class="zigzag-bottom"></div>
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="latest-product">
                  <h2 class="section-title">SẢN PHẨM</h2>
                  <div class="product-carousel">
                    @foreach($mobiles as $mobile)
                      <div class="single-product">
                          <div class="product-f-image">

                              <img src="{!!asset('img/dienthoai/'.$mobile->image)!!}" alt="">
                              <div class="product-hover">
                                  <a href="{!!  route('add-to-cart', ['id'=> $mobile->id]) !!}" class="add_to_cart_button add-to-cart-link "><i class="fa fa-shopping-cart"></i> Đưa vào giỏ</a>
                                  <a href="{!! route('mobile_details', ['id' => $mobile->id]) !!}" class="view-details-link"><i class="fa fa-link"></i> Xem chi tiết</a>
                              </div>
                          </div>

                          <h2>  <a href="{!! route('mobile_details', ['id' => $mobile->id]) !!}">{!! $mobile->name !!}</a></h2>

                          <div class="product-carousel-price">
                                                    <ins>{!! number_format($mobile->price,0) !!} VNĐ</ins>
                              <p class="qty">SỐ LƯỢNG: <span class="current_qty">{{ $mobile->quantity_left }}</span></p>
                          </div>
                      </div>
                      @endforeach
                  </div>
              </div>
          </div>
      </div>
  </div>
</div> <!-- End main content area -->

<div class="brands-area">
  <div class="zigzag-bottom"></div>
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="brand-wrapper">
                  <div class="brand-list">
                      <img src="{!! asset('img/brand1.png') !!}" alt="">
                      <img src="{!! asset('img/brand2.png') !!}"  alt="">
                      <img src="{!! asset('img/brand3.png') !!}"  alt="">
                      <img src="{!! asset('img/brand4.png') !!}"  alt="">
                      <img src="{!! asset('img/brand5.png') !!}"  alt="">
                      <img src="{!! asset('img/brand6.png') !!}"  alt="">

                  </div>
              </div>
          </div>
      </div>
  </div>
</div> <!-- End brands area -->

<div class="product-widget-area">
  <div class="zigzag-bottom"></div>
  <div class="container">
      <div class="row">
          <div class="col-md-6">
              <div class="single-product-widget">
                  <h2 class="product-wid-title">Top Bán chạy</h2>
                  @foreach($topbanchay as $item)
                  <div class="single-wid-product">
                      <a href="{!! route('mobile_details',['id' => $item->mobile->id]) !!}"><img src="{!!asset('img/dienthoai/'.$item->mobile->image)!!}"  class="product-thumb" alt=""></a>
                      <h2><a href="{!! route('mobile_details',['id' => $item->mobile->id]) !!}">{{ $item->mobile->name}}</a></h2>
                      <div class="product-wid-price">
                          <ins>{!! number_format($item->mobile->price,0) !!} VNĐ</ins>
                      </div>
                  </div>
                  @endforeach
              </div>
          </div>

          <div class="col-md-6">
              <div class="single-product-widget">
                  <h2 class="product-wid-title">Sản Phẩm mới nhất</h2>
                  @foreach($latest_mobiles as $item)
                  <div class="single-wid-product">
                      <a href="{!! route('mobile_details',['id' => $item->id]) !!}"><img src="{!!asset('img/dienthoai/'.$item->image)!!}"  class="product-thumb" alt=""></a>
                      <h2><a href="{!! route('mobile_details',['id' => $item->id]) !!}">{{ $item->name}}</a></h2>
                      <div class="product-wid-price">
                        <ins>{!! number_format($item->price,0) !!} VNĐ</ins>
                      </div>
                  </div>
                  @endforeach
              </div>
          </div>
      </div>
  </div>
</div> <!-- End product widget area -->

@endsection
