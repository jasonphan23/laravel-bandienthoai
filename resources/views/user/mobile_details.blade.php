
@extends('layout.master')
@section('active-nav')
<li class=""><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li ><a href="/gioithieu">Về chúng tôi</a></li>
<li><a href="/lienhe">Liên hệ</a></li>

@endsection
@section('content')

<div class="single-product-area">
      <div class="zigzag-bottom"></div>
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="product-content-right">
                       <div class="product-breadcroumb">
                         <a href="{!! route('home') !!}">Trang chủ</a>
                         <a href="{!! route('mobile') !!}">Điện thoại</a>
                          <a href="{!! route('brand_details', ['id' => $mobile_details->brand->id]) !!}">{{$mobile_details->brand->name}}</a>
                          <a href="{!! route('mobile_details', ['id' => $mobile_details->id]) !!}">{{$mobile_details->name}}</a>
                      </div>

                      <div class="row">
                          <div class="col-sm-4">
                              <div class="product-images">
                                  <div class="product-main-img">
                                      <img src="{!!asset('img/dienthoai/'.$mobile_details->image)!!}" alt="">
                                  </div>

                                  <div class="product-gallery">
                                      <img src="img/product-thumb-1.jpg" alt="">
                                      <img src="img/product-thumb-2.jpg" alt="">
                                      <img src="img/product-thumb-3.jpg" alt="">
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-8">
                              <div class="product-inner">
                                  <h2 class="product-name">{{$mobile_details->name}}</h2>
                                  <div class="product-inner-price">
                                      <ins>ĐƠN GIÁ: {{ number_format($mobile_details->price,0) }} VNĐ</ins>
                                        <p class="qty">SỐ LƯỢNG: <span class="current_qty">{{ $mobile_details->quantity_left }}</span></p>
                                  </div>

                                  <div class="product-option-shop">
                                      <a class="add_to_cart_button" data-quantity="1" data-product_sku="" data-product_id="{{ $mobile_details->id}}" rel="nofollow" href="{!!  route('add-to-cart', ['id'=> $mobile_details->id]) !!}">Đưa vào giỏ</a>
                                  </div>


                                  <div role="tabpanel">
                                      <ul class="product-tab" role="tablist">
                                          <li role="presentation" class="active"><a href="#desc" aria-controls="home" role="tab" data-toggle="tab">Mô tả</a></li>
                                          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Thông số kĩ thuật</a></li>
                                      </ul>
                                      <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane fade in active" id="desc">
                                              <h2>Thông tin điện thoại</h2>
                                              {!! $mobile_details->desc !!}
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade" id="profile">
                                              <h2>Thông số Kĩ thuật</h2>
                                                <table class="table">
                                                  <tbody>
                                                    <tr>
                                                    <th>Hệ Điều Hành</th>
                                                    <td>{{$mobile_details->platform }}</td>
                                                    </tr>
                                                    <tr>
                                                     <th>CPU</th>
                                                        <td>{{$mobile_details->cpu }}</td>
                                                    </tr>
                                                    <tr>
                                                     <th>RAM</th>
                                                         <td>{{$mobile_details->ram }}</td>
                                                    </tr>
                                                    <tr>
                                                     <th>Bộ nhớ trong</th>
                                                         <td>{{$mobile_details->internal_memory }}</td>
                                                    </tr>
                                                    <tr>
                                                     <th>Dung lượng pin</th>
                                                         <td>{{$mobile_details->battery }}</td>
                                                    </tr>
                                                    <tr>
                                                     <th>Camera trước</th>
                                                         <td>{{$mobile_details->primary_cam }}</td>
                                                    </tr>
                                                    <tr>
                                                     <th>Camera Sau</th>
                                                         <td>{{$mobile_details->secondary_cam }}</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </div>

                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>


                      <div class="related-products-wrapper">
                          <h2 class="related-products-title">SẢN PHẨM LIÊN QUAN</h2>
                          <div class="related-products-carousel">
                            @foreach($related_products as $repo)
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="{!!asset('img/dienthoai/'.$repo->image)!!}" alt="">
                                    <div class="product-hover">
                                        <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Đưa vào giỏ</a>
                                        <a href="" class="view-details-link"><i class="fa fa-link"></i> Xem Chi Tiết</a>
                                    </div>
                                </div>

                                <h2><a href="">{{ $repo->name }}</a></h2>

                                <div class="product-carousel-price">
                                  <ins>ĐƠN GIÁ: {{  number_format($repo->price, 0) }} VNĐ</ins>
                                </div>
                            </div>

                            @endforeach



                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
