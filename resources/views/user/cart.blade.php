@extends('layout.master')
@section('active-nav')
<li><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li><a href="#">Về chúng tôi</a></li>
<li><a href="#">Liên hệ</a></li>

@endsection
@section('content')


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-content-right">
                    <div class="woocommerce">
                        <form method="post" action="#">
                              {{-- Danh sach cart section --}}
                            <table cellspacing="0" class="shop_table cart">
                                <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name">Sản Phẩm</th>
                                        <th class="product-price">Đơn giá</th>
                                        <th class="product-quantity">SL</th>
                                        <th class="product-subtotal">Tổng cộng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach(Cart::content() as $product)
                                    <tr class="cart_item">
                                        <td class="product-remove">
                                            <a title="Remove this item" class="remove" href="{!! route('remove',['id'=>$product->rowId]) !!}">×</a>
                                        </td>

                                        <td class="product-thumbnail">
                                          @if($product->options->has('image'))
                                            <a href=""><img width="300" height="300" alt="poster_1_up" class="shop_thumbnail" src="{!! asset('img/dienthoai/'.$product->options->image) !!}"></a>
                                            @endif
                                        </td>

                                        <td class="product-name">
                                            <a href="{{ route('mobile_details',['id'=>$product->id]) }}">{{$product->name}}</a>
                                        </td>

                                        <td class="product-price">
                                            <span class="amount">{{number_format($product->price,0)}} VNĐ</span>
                                        </td>

                                        <td class="product-quantity">
                                            <div class="quantity buttons_added">
                                                <a href="{!!  route('minus-to-cart', ['id'=>$product->rowId]) !!}"><input type="button" class="minus" value="-" ></a>
                                                <input type="number" size="4" class="input-text qty text" title="Qty" value="{{ $product->qty }}" min="0" step="1">
                                                <a href="{!!  route('plus-to-cart', ['id'=>$product->rowId]) !!}"><input type="button" class="plus" value="+"></a>
                                            </div>
                                        </td>

                                        <td class="product-subtotal">
                                            <span class="amount"> {{number_format($product->subtotal,0)}} VNĐ</span>
                                        </td>
                                    </tr>

                                    @endforeach

                                    {{-- Tong Tien section --}}

                                    <td class="text-right" colspan="6">
                                    <span style="float:right">Tổng tiền: <strong id="cart_total"> {{ number_format(Cart::total(),0) }} VNĐ </strong> </span>
                                    </td>
                                    <tr>
                                      <td class="actions" colspan="6">
                                        <div class="row">
                                          <div class="col-md-12 text-right">
                                            <a href="{{ route('thanhtoan') }}" name="proceed" class="btn btn-success" id="thanhtoan">THANH TOÁN</a>
                                        </div>
                                      </td>
                                    </tr>
                                </tbody>
                                @if (Session::has('error'))
                                <div class="alert alert-danger" id="baoloi">
                                  Không có giỏ hàng
                                </div>
                                @endif
                            </table>
                        </form>



                            </section>
                        </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
