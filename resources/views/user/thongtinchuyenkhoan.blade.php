@extends('layout.master')
@section('active-nav')
<li class=""><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li ><a href="/gioithieu">Về chúng tôi</a></li>
<li><a href="/lienhe">Liên hệ</a></li>

@endsection
@section('content')


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

              @if(Session::has('error'))
<div class="alert alert-danger">
<strong>Thanh toán không thành công!</strong> {{ Session::get('error') }}
</div>
              @endif

              @if(Session::has('success'))
              <div class="alert alert-success">
            <strong>Thanh toán thành công!</strong> {{ Session::get('success') }}
            </div>
            @endif

                <a href="{{route('home')}}" class="btn btn-success"> TRỞ VỀ TRANG CHỦ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
