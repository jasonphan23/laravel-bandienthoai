@extends('layout.master')
@section('active-nav')
<li><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li><a href="#">Về chúng tôi</a></li>
<li><a href="#">Liên hệ</a></li>

@endsection
@section('content')

<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
              <form action="{{ route('postthanhtoan')}}" method="post">
              <h3 id="order_review_heading">THÔNG TIN ĐƠN HÀNG</h3>
              {!! csrf_field() !!}
              <table cellspacing="0" class="shop_table cart">
                  <thead>
                      <tr>
                          <th class="product-thumbnail">&nbsp;</th>
                          <th class="product-name">Sản Phẩm</th>
                          <th class="product-price">Đơn giá</th>
                          <th class="product-quantity">SL</th>
                          <th class="product-subtotal">Tổng cộng</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach(Cart::content() as $product)
                      <tr class="cart_item">

                          <td class="product-thumbnail">
                            @if($product->options->has('image'))
                              <a href=""><img width="300" height="300" alt="poster_1_up" class="shop_thumbnail" src="{!! asset('img/dienthoai/'.$product->options->image) !!}"></a>
                              @endif
                          </td>

                          <td class="product-name">
                              <a href="single-product.html">{{$product->name}}</a>
                          </td>

                          <td class="product-price">
                              <span class="amount">{{$product->price}}</span>
                          </td>

                          <td class="product-quantity">
                              {{ $product->qty}}
                          </td>

                          <td class="product-subtotal">
                              <span class="amount"> {{$product->subtotal}}</span>
                          </td>
                      </tr>

                      @endforeach

                      {{-- Tong Tien section --}}

                      <td class="text-right" colspan="6">
                      <span style="float:right">Tổng tiền: <strong> {{ Cart::total() }} VNĐ </strong> </span>

                      </td>
                  </tbody>
                        <span style="color:red"><strong>(*) Miễn Phí Ship cho mọi đơn hàng</strong></span>
              </table>

                           <div id="order_review" style="position: relative;">
                              <h3 id="order_review_heading">THÔNG TIN KHÁCH HÀNG</h3>
                              <a href="{{ route('thongtinuser',['id'=>Auth::user()->id]) }}">Thay đổi thông tin</a>
                               <table class="shop_table">
                                   <tbody>
                                       <tr class="cart_item">
                                           <td class="product-name">
                                             Tên Khách hàng  </td>
                                           <td class="product-total">
                                               <span class="amount">{{ Auth::user()->real_name }} </span> </td>
                                       </tr>
                                       <tr class="cart_item">
                                           <td class="product-name">
                                             Địa chỉ  </td>
                                           <td class="product-total">
                                               <span class="amount">{{ Auth::user()->address }} </span> </td>
                                       </tr>
                                       <tr class="cart_item">
                                           <td class="product-name">
                                             Số Điện thoại </td>
                                           <td class="product-total">
                                               <span class="amount">{{ Auth::user()->phone }} </span> </td>
                                       </tr>
                                   </tbody>
                               </table>


                               <div id="payment">
                                   <ul class="payment_methods methods">
                                       <li class="payment_method_bacs">
                                           <input type="radio" data-order_button_text="" checked="checked" value="cod" name="loai_thanh_toan" class="input-radio" id="payment_method_bacs">
                                           <label for="payment_method_bacs">Thanh toán khi nhận hàng (COD)</label>
                                           <div class="payment_box payment_method_bacs">
                                               <p> Thanh toán sau khi hàng đã đến tận tay của bạn.</p>
                                           </div>
                                       </li>
                                       <li class="payment_method_paypal">
                                           <input type="radio" data-order_button_text="Proceed to PayPal" value="paypal" name="loai_thanh_toan" class="input-radio" id="payment_method_paypal">
                                            <label for="payment_method_bacs">Thanh toán bằng Paypal</label>
                                           <div class="payment_box payment_method_paypal">
                                               <p>Thanh toán bằng Paypal, thanh toán trước khi nhận hàng </p>
                                           </div>
                                       </li>
                                   </ul>
                                   <div class="form-row place-order">

                                          <input type="submit" data-value="XÁC NHẬN" value="XÁC NHẬN" id="place_order" name="woocommerce_checkout_place_order" class="button alt">


                                      </div>
                                   <div class="clear"></div>
</form>
                               </div>
                           </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
