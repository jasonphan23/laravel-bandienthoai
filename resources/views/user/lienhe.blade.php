@extends('layout.master')
@section('active-nav')
<li class=""><a href="{{ route('home') }}">Trang Chủ</a></li>
<li class="dropdown"><a href="{{ route('mobile') }}">Điện Thoại</a>
  <div class="dropdown-content">
    @foreach($brands as $brand)
        <a href="{!! route('brand_details',['id' => $brand->id]) !!}">{{$brand->name}}</a>
    @endforeach
      </div>
</li>

<li ><a href="/gioithieu">Về chúng tôi</a></li>
<li class="active"><a href="/lienhe">Liên hệ</a></li>

@endsection
@section('content')

<div class="single-product-area">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <legend class="text-center header">Liên hệ chúng tôi</legend>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <p><strong>Địa Chỉ</strong> : 400 Nguyễn Trãi P7 Q5
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                              <p><strong>Điện thoại</strong> :  28405960
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.72618087525!2d106.67088301435018!3d10.755573792335717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752efcff9bc56f%3A0x39f5241dfd98caa8!2zNDAwIE5ndXnhu4VuIFRyw6NpLCBQaMaw4budbmcgNywgUXXhuq1uIDUsIEjhu5MgQ2jDrSBNaW5oLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1512226527914" width="1000" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

                            </div>
                        </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>


@endsection
