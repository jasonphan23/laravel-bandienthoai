<div class="product-breadcroumb">
   <a href="{!! route('home') !!}">Trang chủ</a>
   <a href="{!! route('mobile') !!}">Điện thoại</a>
   @isset($selected_brand)
    <a href="{!! route('brand_details',['id'=>$selected_brand->id]) !!}">{{ $selected_brand->name }}</a>
   @endisset
</div>
@isset($mobiles_count)
<div class="container">
 <h4>Tìm thấy {{ $mobiles_count}} sản phẩm cho <span style="color:#5a88ca;font-size:20px"> {{ $mobilename }} </span></h4>
</div>
@endisset
@foreach($mobiles as $mobile)
  <div class="col-md-3 col-sm-6">
      <div class="single-shop-product">
          <div class="product-upper">
               <a href="{!! route('mobile_details', ['id' => $mobile->id]) !!}">  <img src="{!!asset('img/dienthoai/'.$mobile->image)!!}" alt=""></a>
          </div>
          <h2><a href="{!! route('mobile_details',['id' => $mobile->id]) !!}">{!! $mobile->name !!}</a></h2>
          <div class="product-carousel-price">
              <ins>{!! number_format($mobile->price,0) !!} VNĐ</ins>
              <p class="qty">SỐ LƯỢNG: <span class="current_qty">{{ $mobile->quantity_left }}</span></p>
          </div>

          <div class="product-option-shop">
              <a class="add_to_cart_button" data-quantity="1" data-product_sku="" data-product_id="{{$mobile->id}}" rel="nofollow" href="{!!  route('add-to-cart', ['id'=>$mobile->id]) !!}">Thêm vào giỏ</a>
          </div>
      </div>
  </div>
  @endforeach
  <div class="pagination-bar text-center">
         {{ $mobiles->links() }}
  </div>
