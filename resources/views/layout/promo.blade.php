<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="{{ asset('img/cover-1.png') }}" alt="Los Angeles">
    </div>

    <div class="item">
      <img src="{{ asset('img/cover-2.jpg') }}" alt="Chicago">
    </div>

    <div class="item">
      <img src="{{ asset('img/cover-3.jpg') }}" alt="New York">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="promo-area">
      <div class="zigzag-bottom"></div>
      <div class="container">
          <div class="row">
              <div class="col-md-3 col-sm-6">
                  <div class="single-promo promo1">
                      <i class="fa fa-refresh"></i>
                      <p>Trả hàng trong 30 ngày</p>
                  </div>
              </div>
              <div class="col-md-3 col-sm-6">
                  <div class="single-promo promo2">
                      <i class="fa fa-truck"></i>
                      <p>Giao hàng miễn phí nhanh chóng</p>
                  </div>
              </div>
              <div class="col-md-3 col-sm-6">
                  <div class="single-promo promo3">
                      <i class="fa fa-lock"></i>
                      <p>Giao dịch an toàn bảo mật</p>
                  </div>
              </div>
              <div class="col-md-3 col-sm-6">
                  <div class="single-promo promo4">
                      <i class="fa fa-gift"></i>
                      <p>Đem đến những sản phẩm mới nhất</p>
                  </div>
              </div>
          </div>
      </div>
  </div> <!-- End promo area -->
