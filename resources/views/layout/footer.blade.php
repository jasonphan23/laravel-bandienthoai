<div class="footer-top-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer-about-us">
                    <h2>Freedom<span>Mobile</span></h2>
                    <p>Nơi cung cấp cho tín đồ đam mê điện thoại di động những sản phẩm tinh xảo nhất</p>
                    <p>Địa chỉ: 400 Nguyễn Trãi P7 Q5</p>
                    <p>Điện thoại: 28405960</p>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">Truy Cập Nhanh </h2>
                    <ul>
                        @if(Auth::check())
                         <li><a href="{{ route('thongtinuser',['id'=>Auth::user()->id])}}">Tài Khoản của Tôi </a></li>
                         <li><a href="{{ route('thongtinuser',['id'=>Auth::user()->id])}}">Lịch Sử Mua Hàng</a></li>
                        @else
                          <li><a href="/login">Đăng Nhập</a></li>
                         <li><a href="/register">Đăng Ký</a></li>
                        @endif
                        <li><a href="/cart">Xem giỏ hàng</a></li>
                        <li><a href="/gioithieu">Giới thiệu</a></li>
                        <li><a href="/lienhe">Liên hệ</a></li>
                    </ul>
                </div>
            </div>



            <div class="col-md-6 col-sm-6">
                <div class="footer-newsletter">
                    <h2 class="footer-wid-title">BẢN ĐỒ SHOP FREEDOM MOBILE</h2>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.72618087525!2d106.67088301435018!3d10.755573792335717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752efcff9bc56f%3A0x39f5241dfd98caa8!2zNDAwIE5ndXnhu4VuIFRyw6NpLCBQaMaw4budbmcgNywgUXXhuq1uIDUsIEjhu5MgQ2jDrSBNaW5oLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1512226527914" width="600" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; 2015 uCommerce. All Rights Reserved. <a href="http://www.freshdesignweb.com" target="_blank">freshDesignweb.com</a></p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">
                    <i class="fa fa-cc-paypal"></i>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End footer bottom area -->


</body>
</html>
