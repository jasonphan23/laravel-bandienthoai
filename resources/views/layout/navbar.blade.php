<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse col-md-3">
                <ul class="nav navbar-nav">
                  @yield('active-nav')
                </ul>
            </div>
            <div class="col-md-4 pull-right">
            <form action="{{route('search-mobile')}}" method="GET">
                  <div class="input-group  search-1bar" id="search_mobile_name_bar" >
               <input type="text" class="form-control" placeholder="Bạn tìm gì...?" id="mobile_name" name="mobile_name"/>
               <div class="input-group-btn">
                    <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                    </button>
               </div>
               </div>
                </div>
            </form>
          </div>
          @yield('mobile-filter')
        </div>
    </div>
</div> <!-- End mainmenu area -->
