<div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title">CHI TIẾT ĐƠN HÀNG</h4>
     </div>
     <div class="modal-body">
<table cellspacing="0" class="shop_table cart">
    <thead>
        <tr>
            <th class="product-name">Sản Phẩm</th>
            <th class="product-price">Đơn giá</th>
            <th class="product-quantity">SL</th>
            <th class="product-subtotal">Tổng cộng</th>
        </tr>
    </thead>
    <tbody>
      @foreach($order_details as $item)
        <tr class="cart_item">

            <td class="product-name">
                <a href="{!! route('mobile_details',['id' => $item->mobile->id]) !!}">{{$item->mobile->name}}</a>
            </td>

            <td class="product-price">
                <span class="amount">{{number_format($item->price,0)}} VNĐ</span>
            </td>

            <td class="product-quantity">
                {{ $item->quantity}}
            </td>

            <td class="product-subtotal">
                <span class="amount"> {{number_format($item->total_price,0)}} VNĐ</span>
            </td>
        </tr>

@endforeach
</tbody>
</table>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
      </div>
