@extends('thukho.master-layout.master-layout')
@section('content')
<div class="row">
                  <div class="col-sm-12">
                      <div class="white-box">
                        @if(Session::has('error'))
          <div class="alert alert-danger">
          <strong>Nhập hàng thất bại!!! {{Session::get('error')}} </strong>
          </div>
                        @endif

                        @if(Session::has('success'))
                        <div class="alert alert-success">
                      <strong>{{Session::get('success')}}!</strong>
                      </div>
                      @endif

                          <h3 class="box-title">CHỌN SẢN PHẨM</h3>
                          <form class="form-horizontal" method='post' action="{{ route('postnhaphang') }}">
  <div class="form-group">
    <label class="control-label col-sm-2" for="tags">Nhập Tên Sản Phẩm</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="tags" placeholder="Nhập vào tên sản phẩm">
    </div>
  </div>
  <div class="form-group">
      <label class="control-label col-sm-2" for="qty"> Nhập SL: </label>
      <div class="col-sm-3">
       <input type="number" id="qty" name="qty" min="1" value="1" max="100" class="form-control">
     </div>
  </div>

  <input id="txtID" type="hidden">
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="button" id="btn_themsp" class="btn btn-default">Thêm vào</button>
    </div>
  </div>


<h2>DANH SÁCH NHẬP HÀNG</h2>

  <table class="table">
    <thead>
      <tr>
        <th>Tên Sản Phẩm</th>
        <th>SL Thêm</th>
        <th>Xóa</th>
      </tr>
    </thead>
    <tbody id="table-body">
      @foreach(Session::get('items_list') as $key=>$product)
      <tr>
    <td>{{ $product['item_name'] }}</td>
    <td>{{ $product['qty'] }}</td>
    <td> <i class="fa fa-trash-o xoasp" aria-hidden="true" name="{{ $product['item_id']}}"></i>  </td>
    </tr>
    @endforeach
    </tbody>
  </table>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btn_themsp" class="btn btn-default">Nhập hàng</button>
    </div>
  </div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
</form>
</div>
</div>

</div>



<script>
$( function() {

    $( "#tags" ).autocomplete({
      source: 'http://deploy.dev:8080/laysanphamnhaphang',
        select: function (event, ui) {
          $('#txtID').val(ui.item.value);
   event.preventDefault();
   $("#tags").val(ui.item.label);
 },
   minLength:1

    });

    $("#btn_themsp").click(function(e) {
      if($("#tags").val == '')
      {
        alert('không dc rỗng');
      }
      else {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/nhapsp/",
            data: {
                product_id: $('#txtID').val(), // < note use of 'this' here
                product_name: $('#tags').val(),
                qty : $('#qty').val(),
            },
            success: function(result) {
             window.location.reload();
            },
            error: function(result) {
                alert('Có lỗi xảy ra !!!');
            }
        });
    }
  });

  $('.xoasp').click(function(event) {
    event.preventDefault();
    $.ajax({
      url: '/xoaspkhoinhap/',
      type: 'GET',
      data: {
        product_id: $(this).attr('name')
      },
      success: function(result) {
       window.location.reload();
      },
      error: function(result) {
          alert('Có lỗi xảy ra !!!');
      }
  });

  } )
});
</script>
@endsection
