@extends('thukho.layout.layout')
@section('loggin-user')
<ul class="nav navbar-top-links navbar-right">
                        <!-- Authentication Links -->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               {{  Auth::user()->email}}<span class="caret"></span>
                                </a>


                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                          Đăng Xuất
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

        <li>
            <a href="{{ route('suathongtinthukho',['id'=>Auth::user()->id]) }}">
                Thông tin cá nhân
            </a>
        </li>
                                </ul>
                            </li>

                    </ul>
@endsection
@section('menubar')
<li style="padding: 10px 0 0;">
                        <a href="{{route('thukho.index')}}" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i><span class="hide-menu">Trang chủ</span></a>
                    </li>
                    <li>
                        <a href="{{ route('xulidh') }}" class="waves-effect"><i class="fa fa-check fa-fw" aria-hidden="true"></i><span class="hide-menu">Xử Lý Đơn Hàng</span></a>
                    </li>
                    <li>
                        <a href="{{ route('nhaphang')}}" class="waves-effect"><i class="fa fa-book fa-fw" aria-hidden="true"></i><span class="hide-menu">Nhập hàng</span></a>
                    </li>
                    <li>
                        <a href="{{ route('xemtonkho')}}" class="waves-effect"><i class="fa fa-mobile fa-fw" aria-hidden="true"></i><span class="hide-menu">Xem tồn kho</span></a>
                    </li>

@endsection
