@extends('thukho.master-layout.master-layout')
@section('content')
<div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                      @if(session('success'))
                              <p class="alert alert-success">{{session('success')}}</p>
                      @endif
                      @if(session('error'))
                              <p class="alert alert-danger">{{session('error')}}</p>
                      @endif

                                        @if ($errors->any())
                                          <div class="alert alert-danger">
                                              <ul>
                                                  @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                  @endforeach
                                              </ul>
                                          </div>
                                      @endif

                                      <form class="form-horizontal form-material">
                                <h2>THÔNG TIN CÁ NHÂN</h2>
                      <div class="form-group">
                          <label class="col-md-12">Tên Đăng Nhập</label>
                          <div class="col-md-12">
                              <input type="text" value="{{ $user->user_name }}" class="form-control form-control-line" disabled> </div>
                      </div>
                        <div class="form-group">
                            <label class="col-md-12">Full Name</label>
                            <div class="col-md-12">
                                <input type="text" value="{{ $user->real_name}}" class="form-control form-control-line"  disabled> </div>
                        </div>
                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Email</label>
                            <div class="col-md-12">
                                <input type="email" value="{{ $user->email }}" class="form-control form-control-line" name="example-email" id="example-email"  disabled> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Số Điện Thoại</label>
                            <div class="col-md-12">
                                <input type="text" value="{{ $user->phone }}" class="form-control form-control-line" disabled > </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Địa chỉ</label>
                            <div class="col-md-12">
                                <input type="text" value="{{ $user->address }}" class="form-control form-control-line" disabled> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Ngày tạo</label>
                            <div class="col-md-12">
                                <input type="text" value="{{ $user->created_at }}" class="form-control form-control-line" disabled> </div>
                        </div>

                    </form>


                    </div>
                    <div class="white-box">
                      <h2>ĐỔI MÂT KHẨU</h2>
                        <form class="form-horizontal form-material" method="post" action="{{ route('suapassword')}}">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group">
                              <label class="col-md-12">Password mới</label>
                              <div class="col-md-12">
                                  <input type="password" value="" class="form-control form-control-line" name="password" required> </div>
                          </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Nhập lại Password mới</label>
                                <div class="col-md-12">
                                    <input type="password" value="" class="form-control form-control-line" name="password_confirmation" required > </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Nhập password cũ</label>
                                <div class="col-md-12">
                                    <input type="password" value="" class="form-control form-control-line" name="old_password" required> </div>
                            </div>
                              <div class="form-group">
                            <div class="col-sm-12">
                                    <input type="submit" class="btn btn-success" value="Cập Nhật">
                            </div>
                          </div>
                        </form>
                    </div>


            </div>
            <!-- /.row -->
@endsection
