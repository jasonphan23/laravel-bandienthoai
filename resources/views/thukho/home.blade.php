@extends('thukho.master-layout.master-layout')
@section('content')
  <h3>Chào mừng bạn đã quay trở lại ! </h3>
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      <div class="white-box">
          <div class="col-in row">
              <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                  <h5 class="text-muted vb">ĐƠN HÀNG TRONG HÔM NAY </h5> </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                  <h3 class="counter text-right m-t-15 text-danger">{{ $today_orders }}</h3> </div>
              <div class="col-md-12 col-sm-12 col-xs-12">

              </div>
          </div>
      </div>
  </div>
  <!-- /.col -->
  <!--col -->
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      <div class="white-box">
          <div class="col-in row">
              <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe01b;"></i>
                  <h5 class="text-muted vb">ĐƠN HÀNG CHƯA XỬ LÝ</h5> </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                  <h3 class="counter text-right m-t-15 text-megna">{{ $cxl_orders }}</h3> </div>
              <div class="col-md-12 col-sm-12 col-xs-12">

              </div>
          </div>
      </div>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      <div class="white-box">
          <div class="col-in row">
              <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe00b;"></i>
                  <h5 class="text-muted vb">NHẬP HÀNG</h5> </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                  <h3 class="counter text-right m-t-15 text-primary"><i class="fa fa-files-o" aria-hidden="true"></i></h3> </div>
              <div class="col-md-12 col-sm-12 col-xs-12">

              </div>
          </div>
      </div>
  </div>
  </div>
@endsection
