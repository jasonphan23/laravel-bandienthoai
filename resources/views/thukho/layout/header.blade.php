<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('thukhocssjs/images/favicon.png') }}">
    <title>Pixel Admin - Responsive Admin Dashboard Template built with Twitter Bootstrap</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('thukhocssjs/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('thukhocssjs/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('thukhocssjs/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('thukhocssjs/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ asset('thukhocssjs/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('thukhocssjs/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('thukhocssjs/css/jquery_ui.css') }}" rel="stylesheet">
      <link href="{{ asset('thukhocssjs/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('thukhocssjs/css/colors/blue-dark.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('thukhocssjs/css/datatable.css') }}"rel="stylesheet">
    <!-- jQuery -->
<script src="{{ asset('thukhocssjs/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('thukhocssjs/js/jquery_ui.js') }}"></script>
<!-- Latest jQuery form server -->
   <script src="https://code.jquery.com/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('thukhocssjs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('thukhocssjs/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ asset('thukhocssjs/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->

<!--Counter js -->

<!--Morris JavaScript -->

<!-- Custom Theme JavaScript -->


<script src="{{ asset('thukhocssjs/bower_components/toast-master/js/jquery.toast.js') }}"></script>
<script src="{{ asset('thukhocssjs/js/datatable.js') }}"></script>
<meta name="csrf_token" content="{{ csrf_token() }}" />
<script src="{{ asset('thukhocssjs/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<script type="text/javascript">
$(document).ready(function() {
    $.toast({
        heading: 'Welcome to Pixel admin',
        text: 'Use the predefined ones, or specify a custom position object.',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'info',
        hideAfter: 3500,
        stack: 6
    });
    $('#myTable').DataTable({
        "order": []
       });
    $('#myTable_deal').DataTable({
        "order": [[ 7, 'desc' ]]
       });
    $('#bangchi').DataTable({
        "order": []
       });
    $('#bangthu').DataTable({
        "order": []
       });

});
</script>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                <div class="top-left-part"><a class="logo" href="{{route('home')}}"><b><img src="{{ asset('thukhocssjs/images/pixeladmin-logo.png') }}" alt="home" /></b><span class="hidden-xs"><img src="{{ asset('thukhocssjs/images/pixeladmin-text.png') }}" alt="home" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left m-l-20 hidden-xs">
                    <li>

                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                    @yield('loggin-user')
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
