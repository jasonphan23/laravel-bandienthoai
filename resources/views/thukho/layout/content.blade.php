<!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">@yield('title_page')</h4> </div>
              @yield('content')
           </div>
           <!-- /.container-fluid -->
           <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
       </div>
       <!-- /#page-wrapper -->
   </div>
 </body>
  </html>
