<div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title">CHI TIẾT ĐƠN HÀNG</h4>
     </div>
     <div class="modal-body">


         <h2>THÔNG TIN KHÁCH HÀNG</h2>
         <table class="table">
           <thead>
             <tr>
               <tr>
                   <th class="product-name">Tên Khách Hàng</th>
                   <th class="product-price">Địa Chỉ</th>
                   <th class="product-quantity">Email</th>
                   <th class="product-subtotal">SĐT</th>
                   <th class="product-subtotal">Loại Thanh Toán</th>
               </tr>
             </tr>
           </thead>
           <tbody>
               <tr class="cart_item">

                   <td class="product-name">
                     {{$order->user->real_name}}
                   </td>
                   <td class="product-name">
                     {{$order->user->address}}
                   </td>
                   <td class="product-name">
                     {{$order->user->email}}
                   </td>
                   <td class="product-name">
                     {{$order->user->phone}}
                   </td>
                   <td class="product-name">
                     @if($order->loai_thanh_toan==0)
                     COD
                     @else
                     PAYPAL
                     @endif
                   </td>
               </tr>

           </tbody>
         </table>

  <h2>SẢN PHẨM ĐẶT MUA</h2>
  <table class="table">
    <thead>
      <tr>
        <tr>
            <th class="product-name">Sản Phẩm</th>
            <th class="product-price">Đơn giá</th>
            <th class="product-quantity">SL</th>
            <th class="product-subtotal">Tổng cộng</th>
        </tr>
      </tr>
    </thead>
    <tbody>
      @foreach($order_details as $item)
        <tr class="cart_item">

            <td class="product-name">
                <a href="{{ route('mobile_details',['id'=>$item->mobile_id])}}">{{$item->mobile->name}}</a>
            </td>

            <td class="product-price">
                <span class="amount">{{$item->price}}</span>
            </td>

            <td class="product-quantity">
                {{ $item->quantity}}
            </td>

            <td class="product-subtotal">
                <span class="amount"> {{$item->total_price}}</span>
            </td>
        </tr>

@endforeach
    </tbody>
  </table>
</div>

<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
      </div>
