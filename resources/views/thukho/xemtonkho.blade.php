@extends('thukho.master-layout.master-layout')
@section('content')
<div class="row">
                  <div class="col-sm-12">
                      <div class="white-box">
                          <h3 class="box-title">DANH SÁCH SẢN PHẨM</h3>
                          <div class="table-responsive">
                          <table id="table_tonkho" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                  <thead>
                                      <tr>
                                        <th>Tên sản phẩm</th>
                                        <th>Thương Hiệu</th>
                                        <th>Số Lượng Tồn</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                      <th>Tên sản phẩm</th>
                                      <th>Thương Hiệu</th>
                                      <th>Số Lượng Tồn</th>
                                    </tr>
                                  </tfoot>
                                  <tbody>
                                  @foreach($mobiles as $m)

                                  <tr>


                                      <td><a href="{{ route('mobile_details',['id'=>$m->id]) }} ">{{ $m->name }}</a></td>
                                      <td>{{ $m->brand->name }}</td>
                                      @if($m->quantity_left<=5)
                                        <td bgcolor="#FF0000" style="color:white">{{ $m->quantity_left }}</td>
                                      @else
                                      <td>{{ $m->quantity_left }}</td>
                                      @endif
                                  </tr>


                                  @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>

              </div>

              <script type="text/javascript">
              $(document).ready(function() {
                $('#table_tonkho').DataTable({
                  "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
            }
                });

              });
              </script>

@endsection
