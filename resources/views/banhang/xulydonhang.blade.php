@extends('banhang.master-layout.master-layout')
@section('content')
<div class="row">
                  <div class="col-sm-12">
                      <div class="white-box">
                          <h3 class="box-title">DANH SÁCH ĐƠN HÀNG</h3>
                          <div class="table-responsive">
                          <table id="table_donhang" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                  <thead>
                                      <tr>
                                        <th>Tên người mua</th>
                                        <th>Điện thoại</th>
                                        <th>Thanh Toán</th>
                                        <th>Ngày Tạo</th>
                                            <th>Tổng Tiền (VNĐ)</th>

                                        <th>Xem Chi Tiết</th>
                                          <th>Trạng Thái</th>
                                        <th>Cập Nhật</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                      <th>Tên người mua</th>
                                      <th>Điện thoại</th>
                                      <th>Thanh Toán</th>
                                      <th>Ngày Tạo</th>
                                          <th>Tổng Tiền (VNĐ)</th>

                                      <th>Xem Chi Tiết</th>
                                        <th>Trạng Thái</th>
                                      <th>Cập Nhật</th>
                                    </tr>
                                  </tfoot>
                                  <tbody>
                                  @foreach($lst_orders as $order)

                                  <tr>


                                      <td>{{ $order->user->real_name }}</td>
                                      <td>{{ $order->user->phone }}</td>
                                      @if($order->loai_thanh_toan==0)
                                      <td>COD</td>
                                      @else
                                      <td>PAYPAL</td>
                                      @endif
                                      <td>{{ $order->created_at }}</td>
                                      <td>{{ $order->summary }}</td>

                                      <td><button class="btn btn-primary xemchitiet" id="{{$order->id}}" data-toggle="modal" data-target="#myModal"> Xem Chi Tiết </button></td>
                                      @if($order->status==0)
                                      <td>
                                        <form action="{{ route('capnhatdonhang',['id'=>$order->id]) }}" method="POST">
                                          <input type="hidden" name="_method" value="PUT">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <select name="status" class="js-example-basic-multiple js-states form-control" id="members" >
                                            <option value="0">Chưa xử lý</option>
                                            <option value="1">Đang xử lý</option>
                                            <option value="2">Đã Hoàn Thành</option>
                                            <option value="-1">Hủy</option>
                                          </select>
                                          <td><button type="submit" class="btn btn-info"> Cập nhật </button></td>
                                        </form>
                                    </td>
                                      @elseif($order->status==1)
                                      <td>
                                      <form action="{{ route('capnhatdonhang',['id'=>$order->id]) }}" method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <select name="status" class="js-example-basic-multiple js-states form-control" id="members" >
                                          <option value="0">Chưa xử lý</option>
                                          <option selected value="1">Đang xử lý</option>
                                          <option value="2">Đã Hoàn Thành</option>
                                          <option value="-1">Hủy</option>
                                        </select>
                                          <td><button type="submit" class="btn btn-info"> Cập nhật </button></td>
                                        </form>
                                      </td>
                                      @elseif($order->status==2)
                                      <td>
                                      <form action="{{ route('capnhatdonhang',['id'=>$order->id]) }}" method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <select name="status" class="js-example-basic-multiple js-states form-control" id="members" >
                                          <option value="0" disabled>Chưa xử lý</option>
                                          <option value="1" disabled>Đang xử lý</option>
                                          <option selected value="2">Đã Hoàn Thành</option>
                                          <option value="-1" disabled>Hủy</option>
                                        </select>
                                          <td><button type="submit" class="btn btn-info"> Cập nhật </button></td>
                                        </form>
                                      </td>
                                      @else
                                      <td>
                                      <form action="{{ route('capnhatdonhang',['id'=>$order->id]) }}" method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <select name="status" class="js-example-basic-multiple js-states form-control" id="members" >
                                          <option value="0" disabled>Chưa xử lý</option>
                                          <option value="1" disabled>Đang xử lý</option>
                                          <option value="2" disabled>Đã Hoàn Thành</option>
                                          <option selected value="-1">Hủy</option>
                                        </select>
                                          <td><button type="submit" class="btn btn-info"> Cập nhật </button></td>
                                        </form>
                                      </td>
                                      @endif

                                  </tr>

                                  @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>

              </div>
              <div id="myModal" class="modal fade" role="dialog">
             <div class="modal-dialog">
               <div class="modal-content" id="cthd-container">
                 @isset($order_details)
                 @include('layout.modalctdh-partial');
                 @endisset
               </div>
             </div>
           </div>
              <script type="text/javascript">
              $(document).ready(function() {
                $('#table_donhang').DataTable({
                  "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json",
            },
                  "order": [[3, "desc"]]
                });
                $(".xemchitiet").click(function(event) {
                   event.preventDefault();
                   var id =  $(this).attr('id');
                   //console.log(id);
                  $.ajax({
                    url: '/xemctdh',
                    type: 'GET',
                    dataType : 'html',
                    data: {order_id: id },
                    success:function(data) {
                      $("#cthd-container").html(data);
                      //console.log('du');
                    }
                })
              })
              });
              </script>

@endsection
