<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//trang users
Route::get('/', 'HomeController@index')->name('home');
Route::get('header',function()
{
  return view('layout.header');
});

Route::get('mobile','MobileController@index')->name('mobile');
Route::get('details/{id}','MobileController@GetDetails')->name('mobile_details');
Route::get('search-mobile','MobileController@Search')->name('search-mobile');
Route::get('brand/{id}','MobileController@GetProductsByBrand')->name('brand_details');
Auth::routes();
Route::get('advanced-search','MobileController@advancedsearch');
Route::get('cart','CartController@index')->name('cart');
Route::get('gioithieu','HomeController@gioithieu');
Route::get('lienhe','HomeController@lienhe');


Route::get('add-to-cart/{id}','CartController@addtocart')->name('add-to-cart');
Route::get('minustocart/{id}','CartController@minustocart')->name('minus-to-cart');
Route::get('plustocart/{id}','CartController@plustocart')->name('plus-to-cart');

Route::get('removefromcart/{id}','CartController@remove')->name('remove');

Route::get('thongtin/{id}','UserController@thongtinuser')->name('thongtinuser');
Route::post('thongtin','UserController@postsuathongtinuser')->name('postsuathongtinuser');
Route::get('sendNewUserMail/{id}/{email}','UserController@sendNewUserMailDone')->name('sendNewUserMail');
Route::post('suapassword','UserController@postsuapassword')->name('suapassword');

Route::get('thanhtoan','OrderController@thanhtoan')->name('thanhtoan');
Route::post('postthanhtoan','OrderController@store')->name('postthanhtoan');
Route::get('trangthaithanhtoan', 'OrderController@getPaymentStatus')->name('paymentstatus');
Route::get('thongtinchuyenkhoan', 'OrderController@getthongtin')->name('thongtinck');

Route::get('xemchitietdonhang', 'OrderController@getctdh')->name('xemchitietdonhang');


//trang admin


Route::group(['middleware' => 'auth'], function () {
  Route::get('admin', function () {
      return view('admin.vendor.adminlte.home');
  })->name('admin');

});

Route::get('admin-register', 'Auth\RegisterController@showAdminRegistrationForm')->name('admin-register');
Route::post('admin-register', 'Auth\RegisterController@register_admin')->name('admin-register');


//trang nhan vien ban hand


Route::group(['prefix' => 'banhang','middleware' => 'auth'], function(){
  Route::get('/', 'UserController@getIndexBanHang')->name('banhang.index');
  Route::get('xulidh', 'OrderController@getListDH')->name('xulidh');
  Route::put('capnhatdonhang/{id}', 'OrderController@capnhatdonhang')->name('capnhatdonhang');
  Route::get('suathongtinnvbh/{id}', 'UserController@ThongTinNVBH')->name('suathongtinnvbh');

});

//trang thu kho

Route::group(['prefix' => 'thukho','middleware' => 'auth'], function(){
  Route::get('/index', 'UserController@getIndexThuKho')->name('thukho.index');
    Route::get('nhaphang', 'NhapHangController@Index')->name('nhaphang');
    Route::post('postnhaphang', 'NhapHangController@PostNhapHang')->name('postnhaphang');
        Route::get('xemtonkho', 'NhapHangController@XemTonKho')->name('xemtonkho');
        Route::get('suathongtinthukho/{id}', 'UserController@ThongTinThuKho')->name('suathongtinthukho');
});


  Route::get('xemctdh', 'OrderController@getctdh_thukho')->middleware('auth')->name('xemctdhtk');
  Route::get('laysanphamnhaphang', 'NhapHangController@getsanpham')->middleware('auth')->name('laysanphamnhaphang');
    Route::get('nhapsp', 'NhapHangController@AddToSession')->middleware('auth')->name('laysanphamnhaphang');
    Route::get('xemsession', 'NhapHangController@xemsession')->middleware('auth')->name('laysanpha');
      Route::get('xoaspkhoinhap', 'NhapHangController@DeleteFromSession')->middleware('auth')->name('xoaspkhoinhap');
    Route::get('test', function(){
    return \Session::get('items_list');
});
