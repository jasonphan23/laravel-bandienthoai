<?php
return[
'client_id' =>'AWQqY-oh35NQtNxeOh2LAp80HVZnWD4hyVYbCOyQmWCGNGJv9K5SSSOZ5Fh6eTu80VhX_YBDo9Vwth7f',
'secret' => 'EGxFlnj6t0PFP4XACbsX32wqF42bREnikaCSg_wUB77TChYxzWB2BZFbTrYjeEVma0RwGpz4_nHqnV_S',
/**
* SDK configuration 
*/
'settings' => [
	/**
	* Available option 'sandbox' or 'live'
	*/
	'mode' => 'sandbox',
	/**
	* Specify the max request time in seconds
	*/
	'http.ConnectionTimeOut' => 1000,
	/**
	* Whether want to log to a file
	*/
	'log.LogEnabled' => true,
	/**
	* Specify the file that want to write on
	*/
	'log.FileName' => storage_path() . '/logs/paypal.log',
	/**
	* Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
	*
	* Logging is most verbose in the 'FINE' level and decreases as you
	* proceed towards ERROR
	*/
	'log.LogLevel' => 'INFO',
],
];