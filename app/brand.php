<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brand extends Model
{
    protected $table = 'brands' ;
    public function mobiles()
    {
      return $this->hasMany('App\mobile');
    }
}
