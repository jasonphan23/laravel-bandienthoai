<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_detail extends Model
{
    protected $table ='order_details';
    protected $fillable = ['order_id','mobile_id','quantity', 'price','total_price'];
	protected $guarded =[];

	 public function orders()
    {
        return $this->belongsTo('App\order');
    }

    public function mobile()
    {
        return $this->belongsTo('App\mobile','mobile_id');
    }
}
