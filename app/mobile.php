<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mobile extends Model
{
    protected $table = 'mobiles' ;
    public function brand()
    {
      return $this->belongsTo('App\brand');
    }
      public function orders()
      {
        return $this->belongsToMany('App\order');
      }
}
