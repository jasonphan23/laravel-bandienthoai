<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\mobile;
use  Gloudemans\Shoppingcart\Facades\Cart;
use App\brand;
use App\Orders_detail;
use App\order;
use DB;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
     public function index()
    {
        $mobiles = mobile::inRandomOrder()->get();
        $latest_mobiles = mobile::orderBy('id','desc')->where('deleted',0)->select('id','name','price','quantity_left','image')->take(4)->get();
        $brands = brand::all();
        $order_banduoc = order::where('status',2)->select('id')->get();
        $top_banchay = Orders_detail::whereIn('order_id', $order_banduoc)->select('mobile_id',DB::raw('SUM(quantity) as sum'))->orderBy('sum','desc')->groupBy('mobile_id')->take(4)->get();

        return view('user.home')->with(['mobiles'=>$mobiles,'brands'=>$brands,'latest_mobiles'=>$latest_mobiles,'topbanchay'=>$top_banchay]);
    }

    public function gioithieu()
   {

       $brands = brand::all();

       return view('user.gioithieu')->with(['brands'=>$brands]);
   }
   public function lienhe()
  {

      $brands = brand::all();

      return view('user.lienhe')->with(['brands'=>$brands]);
  }

}
