<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mobile;
use Session;
class NhapHangController extends Controller
{

    function __construct()
    {

    }

    function Index()
    {
      if(Auth::user()->role==3)
      {
      if(!Session::has('items_list'))
      {
      \Session::put('items_list', array());
    }

      return view('thukho.nhaphang');
    }
    return redirect('/');
    }

    function getsanpham(Request $request)
    {
      $term = $request->term;

      $mobiles = mobile::where('name','LIKE','%'.$term.'%')->get();
      foreach($mobiles as $key=>$value)
      {

        $return_name[] = array ( 'label' => $value->name,
         'value' => $value->id);
      }

      return $return_name;
    }
    function AddToSession(Request $request)
    {

      $keyitem = -1;
      if(Session::has('items_list'))
      {
        $item_list = Session::get('items_list');
        $mobile = mobile::find($request->product_id);
        foreach($item_list as $key => $product)
        {
          if ( $product['item_id'] == $request->product_id )
          {
            $keyitem = $key;
            break;
          }
       }
          if($keyitem!=-1)
          {
          $sex = $item_list[$key]['qty'] + $request->qty;
          $item_list[$key]['qty'] = $sex;
        }
        else {
      array_push($item_list, array(
        "item_id" => $mobile->id,
        "item_name" => $mobile->name,
        "qty" => $request->qty,
    ));
        }

            Session::put('items_list',$item_list);
      }

    }

    function DeleteFromSession(Request $request)
    {

      $keyitem = -1;
      if(Session::has('items_list'))
      {
        $item_list = Session::get('items_list');
        $mobile = mobile::find($request->product_id);
        foreach($item_list as $key => $product)
        {
          if ( $product['item_id'] == $request->product_id )
          {
            $keyitem = $key;
            break;
          }
       }
          unset($item_list[$keyitem]);
        }

            Session::put('items_list',$item_list);
      }

    function PostNhapHang()
    {


          $item_list = Session::get('items_list');
          if(count($item_list)>0)
          {
      foreach($item_list as $key=>$product)
      {
        $mobile = mobile::find($product['item_id']);

        $mobile->quantity_left = $mobile->quantity_left + $product['qty'];
        $mobile->save();
      }
      Session::put('success','Đã nhập thành công');
      Session::forget('items_list');
          return redirect()->back();
    }
    else {
      Session::put('error','Chưa có sản phẩm để cho nhập hàng');
      return redirect()->back();
    }
    }

    function XemTonKho()
    {
        if(Auth::user()->role==3)
        {
        $mobiles = mobile::where('deleted',0)->get();
        return view('thukho.xemtonkho')->with(['mobiles'=>$mobiles]);
      }
      else {
        return redirect()->route('home');
      }
    }
}
