<?php

namespace App\Http\Controllers;

use App\order;
use App\mobile;
use App\Orders_detail;
use Illuminate\Http\Request;
use App\brand;
use  Gloudemans\Shoppingcart\Facades\Cart;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Validator;
use URL;
use Session;
use Redirect;
use Auth;

use Illuminate\Support\Facades\Input;
use DB;

use Config;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private $_api_context;

   public function __construct()
   {
       $this->middleware('auth');
       // setup PayPal api context
       $paypal_conf = Config::get('paypal');
       $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
       $this->_api_context->setConfig($paypal_conf['settings']);
   }
    public function thanhtoan()
    {
      if(Cart::total()==0)
      {
        return redirect()->back()->with('error','Bạn chưa có sản phẩm nào để thanh toán');
      }

      $brands = brand::all();
      return view('user.confirmorder')->with(['brands'=>$brands]);;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->loai_thanh_toan=="cod")
        {
          $order = new order;
          $order->user_id = Auth::user()->id;
          $order->status = 0; //chua xu ly
          $order->summary = Cart::total();
          $order->loai_thanh_toan = 0; // 1 = paypal , 0 = COD
          $order->save();


          foreach(Cart::content() as $product)
          {
            $orders_detail = new Orders_detail();

            DB::table('order_details')->insert(
              [
                  'order_id' => $order->id,
                  'mobile_id'=> $product->id,
                  'quantity' =>   $product->qty,
                  'price' =>  $product->price,
                  'total_price' =>  $product->subtotal,
              ]
          );
          $mobile = mobile::find($product->id);
          $mobile->quantity_left = $mobile->quantity_left - $product->qty;
          $mobile->save();
          }

          Cart::destroy();
          Session::put('success','Payment success');
          return Redirect::route('thongtinck');
        }

        if($request->loai_thanh_toan=="paypal")
        {

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_list = new ItemList();
        $array_item = array();
        foreach(Cart::content() as $product)
        {
          $item = new Item();
          $item->setName($product->name) /** item name **/
              ->setCurrency('USD')
              ->setQuantity($product->qty)
              ->setPrice(($product->price*0.044)/1000); /** unit price **/
         array_push($array_item,$item);
        }

        $item_list->setItems($array_item);
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal((Cart::total()*0.044)/1000);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('paymentstatus')) /** Specify return URL **/
            ->setCancelUrl(URL::route('paymentstatus'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                Session::put('error','Connection timeout');
                return Redirect::route('thongtinck');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                Session::put('error','Some error occur, sorry for inconvenient');
                return Redirect::route('thongtinck');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if(isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        Session::put('error','Unknown error occurred');
        return Redirect::route('thongtinck');
        }
   }

   public function getPaymentStatus()
      {
          /** Get the payment ID before session clear **/
          $payment_id = Session::get('paypal_payment_id');
          /** clear the session payment ID **/
          Session::forget('paypal_payment_id');
          if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
              \Session::put('error','Payment failed empty');
              return Redirect::route('thongtinck');
          }
          $payment = Payment::get($payment_id, $this->_api_context);
          /** PaymentExecution object includes information necessary **/
          /** to execute a PayPal account payment. **/
          /** The payer_id is added to the request query parameters **/
          /** when the user is redirected from paypal back to your site **/
          $execution = new PaymentExecution();
          $execution->setPayerId(Input::get('PayerID'));
          /**Execute the payment **/
          $result = $payment->execute($execution, $this->_api_context);
          /** dd($result);exit; /** DEBUG RESULT, remove it later **/
          if ($result->getState() == 'approved') {

              /** it's all right **/
              /** Here Write your database logic like that insert record or value in database if you want **/

              $order = new order;
              $order->user_id = Auth::user()->id;
              $order->status = 0; //chua xu ly
              $order->summary = Cart::total();
              $order->loai_thanh_toan = 1; // 1 = paypal , 0 = COD
              $order->save();


              foreach(Cart::content() as $product)
              {
                $orders_detail = new Orders_detail();

                DB::table('order_details')->insert(
                  [
                      'order_id' => $order->id,
                      'mobile_id'=> $product->id,
                      'quantity' =>   $product->qty,
                      'price' =>  $product->price,
                      'total_price' =>  $product->subtotal,
                  ]
              );


              $mobile = mobile::find($product->id);
              $mobile->quantity_left = $mobile->quantity_left - $product->qty;
              $mobile->save();
            }
              Cart::destroy();
              Session::put('success','Payment success');
              return Redirect::route('thongtinck');
          }
          else {
            Session::put('error','Payment failed');
            return Redirect::route('thongtinck');
          }

      }
    /**
     * Display the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        //
    }

    public function getthongtin()
    {
        $brands = brand::all();
      return view('user.thongtinchuyenkhoan')->with('brands',  $brands);
    }


      public function getctdh(Request $request)
      {

        $order_details = Orders_detail::where('order_id',$request->order_id)->get();

        return view('layout.modalctdh-partial')->with(['order_details'=>$order_details]);
      }

      public function getListDH()
      {
        if(Auth::user()->role==4)
        {
        $lst_orders = order::orderBy('created_at', 'DESC')->get();
        return view('banhang.xulydonhang')->with(['lst_orders'=>$lst_orders]);
      }
      else {
        return redirect('/');
      }
      }

      public function capnhatdonhang($id,Request $request)
      {

        $order = order::find($id);
        $order->status = $request->status;
        $order->save();

        if($order->status==-1)
        {
          $order_details = Orders_detail::where('order_id',$id)->get();
          foreach($order_details as $product)
          {
            $mobile = mobile::find($product->mobile_id);
            $mobile->quantity_left = $mobile->quantity_left + $product->quantity;
            $mobile->save();
          }
        }
        return redirect()->back();
      }
      public function getctdh_thukho(Request $request)
      {
        if(Auth::user()->role==3)
        {
        $order_details = Orders_detail::where('order_id',$request->order_id)->get();
        $order = order::where('id',$request->order_id)->first();
        return view('thukho.layout.modalctdh-partial')->with(['order_details'=>$order_details,'order'=>$order]);
      }
      else {
        return redirect('/');
      }

      }


}
