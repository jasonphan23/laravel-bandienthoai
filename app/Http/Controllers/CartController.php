<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\brand;
use  Gloudemans\Shoppingcart\Facades\Cart;
use App\mobile;
class CartController extends Controller
{
  function index()
  {
    $brands = brand::all();
  return view('user.cart')->with(['brands'=>$brands]);
  }
    function addtocart($id)
    {
      $mobile = mobile::find($id);
      Cart::add(['id' => $mobile->id, 'name' => $mobile->name, 'qty' => 1, 'price' => $mobile->price,'options'=>['image'=>$mobile->image]]);
      return redirect()->back();
    }
    function minustocart($rowId)
    {
      $cart_QTY = Cart::get($rowId)->qty;
      Cart::update($rowId,$cart_QTY-1);
      return redirect()->back();
    }
    function plustocart($rowId)
    {
      $cart_QTY = Cart::get($rowId)->qty;
      Cart::update($rowId,$cart_QTY+1);
      return redirect()->back();
    }
    function remove($rowId)
    {
      Cart::remove($rowId);
      return redirect()->back();
    }
}
