<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Mail;
use App\brand;
use Hash;
use Validator;
use Illuminate\Support\Facades\Input;
use App\order;
use App\Orders_detail;
use App\Http\Mail\verifyNewEmailUser;
class UserController extends Controller
{
  function __construct()
  {
    $this->middleware('auth');
  }
  function thongtinuser($id)
  {
          $brands = brand::all();
        $user = User::find($id);
    $lst_donhang = order::with('Order_details')->where('user_id',$id)->get();
    return view('user.thongtin')->with(['user'=>$user,'lst_donhang'=>$lst_donhang,'brands'=>$brands]);
  }

  function ThongTinThuKho($id)
  {
    if(Auth::user()->role==3)
    {
    $user = User::find($id);
    return view('thukho.suathongtin')->with(['user'=>$user]);
  }
  }

  function ThongTinNVBH($id)
  {
    if(Auth::user()->role==4)
    {
    $user = User::find($id);
    return view('banhang.suathongtin')->with(['user'=>$user]);
  }
  }




  function postsuathongtinuser()
  {
      $id = Auth::user()->id;
      $user = User::find($id);
      $rules = array(
            'email'=> 'unique:users',
            'phone'=>'unique:users'
          );
      if($user->email == Input::get('email'))
      {
        unset($rules['email']);
      }
      if($user->phone == Input::get('phone'))
      {
          unset($rules['phone']);
      }
    $validator = Validator::make(Input::all(), $rules);

    // process
    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator);
    }
    else
    {
    $id = Auth::user()->id;
    $user = User::find($id);
    $user->real_name = Input::get('real_name');
    $user->address = Input::get('address');
    $user->phone = Input::get('phone');
    $user->save();
    $new_email = Input::get('email');

    if($new_email != $user->email)
    {
      $this->sendEmail($user,$new_email);
      return redirect()->back()->with('status','Đã Cập Nhật Thông Tin !!! Thư xác nhận email mới đã được gửi đi. Yêu cầu xác nhận Email');
    }
    else
    {
        return redirect()->back()->with('status','Thông tin cá nhân đã được cập nhật');
    }

  }
}
  public function sendEmail($thisUser,$new_email)
  {
        Mail::to($new_email)->send(new verifyNewEmailUser($thisUser,$new_email));
  }
  public function sendNewUserMailDone($id,$email)
     {
         $u = User::find($id);
         if($u)
         {
             $u->email = $email;
             $u->save();
             return redirect('thongtin/'.$id)->with('success','Đã Xác Nhận Email mới Thành Công !!!!!');
         }
         else{
             return redirect('thongtin/'.$id)->with('fail','Lỗi xác nhận');
         }

     }
     function postsuapassword()
       {
           $rules = array(
                 'password' => 'required|string|min:6|confirmed',
                 'old_password' => 'required|string|min:6',
               );
           $validator = Validator::make(Input::all(), $rules);

           // process
           if ($validator->fails()) {
               return redirect()->back()
                   ->withErrors($validator);
           }
           else
           {
           $id = Auth::user()->id;
           $webmaster = User::where('id',$id)->first();
           if(!Hash::check(Input::get('old_password'), Auth::user()->password))
           {
             return redirect()->back()->with('error','Nhập không đúng password hiện tại!');
           }
           else {
             $webmaster->password = bcrypt(Input::get('password'));
             $webmaster->save();
             return redirect()->back()->with('success','Đã Đổi Mật Khẩu Thành Công !!!!!');
           }
         }
       }
       function getIndexThuKho()
       {
         if(Auth::user()->role == 3)
         {
         return view('thukho.home');
        }
        else {
          return redirect('/');
        }
       }

       function getIndexBanHang()
       {
         if(Auth::user()->role == 4)
         {
         $today_orders = order::where('created_at','>=', date('Y-m-d').' 00:00:00')->count();
         $cxl_orders = order::where('status',0)->count();
         return view('banhang.home')->with(['today_orders'=>$today_orders,'cxl_orders'=>$cxl_orders]);
        }
        else {
          return redirect('/');
        }
       }

}
