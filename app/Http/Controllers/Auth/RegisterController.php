<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function RegistrationForm()
    {
      return view('user.auth.register');
    }
    public function showAdminRegistrationForm()
    {
        return view('adminlte::auth.register');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_name'     => 'required|max:255|unique:users',
            'real_name' => 'max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|unique:users',
            'address'=>'required'
        ]);
    }
    protected function validator_admin(array $data)
    {
        return Validator::make($data, [
            'user_name'     => 'required|max:255|unique:users',
            'real_name' => 'max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'terms'    => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

      protected function create(array $data)
     {
         return User::create([
             'user_name' => $data['user_name'],
             'email' => $data['email'],
             'real_name' => $data['name'],
             'phone'=>$data['phone'],
             'address' => $data['address'],
             'password' => bcrypt($data['password']),
             'role'=>1,
         ]);
     }

    protected function create_admin(array $data)
    {
        $fields = [
            'user_name'     => $data['user_name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
            'role'=>2,
        ];
        if (config('auth.providers.users.field','email') === 'username' && isset($data['username'])) {
            $fields['user_name'] = $data['user_name'];
        }
        return User::create($fields);
    }
}
