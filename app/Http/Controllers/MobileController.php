<?php

namespace App\Http\Controllers;

use App\mobile;
use App\brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $mobiles = mobile::paginate(12);
      $brands = brand::all();


      return view('user.mobile')->with(['mobiles'=>$mobiles,'brands'=>$brands]);
    }
    public function GetDetails($id)
    {
      $brands = brand::all();
      $mobile_details = mobile::where('id',$id)->first();
      $related_products = mobile::where('id','!=',$id)->orderByRaw('RAND()')->take(5)->get();
      return view('user.mobile_details')->with(['mobile_details'=>  $mobile_details,'related_products'=>$related_products,'brands'=>$brands]);
    }
    public function GetProductsByBrand($id)
    {
      $mobiles = mobile::where('brand_id',$id)->paginate(12);
          $brands = brand::all();
          $selected_brand = brand::find($id);
      return view('user.mobile')->with(['mobiles'=>$mobiles,'brands'=>$brands,'selected_brand'=>$selected_brand]);
    }
    public function Search()
    {
            $brands = brand::all();
      $mobile_name = Input::get('mobile_name');
      $mobiles = mobile::where('name','Like','%'.$mobile_name.'%')->paginate(12);
      $mobiles_count = $mobiles->count();
      return view('user.mobile')->with(['mobiles'=>$mobiles,'mobiles_count'=>$mobiles_count,'mobilename'=>$mobile_name,'brands'=>$brands]);
    }

    public function advancedsearch(Request $request)
    {
      $cost = $request->cost;
      $brand = $request->brand;
      $mobiles = null;

      if($cost == "all")
      {
      if($brand!="")
        {
          $mobiles = mobile::where('brand_id',$brand)->paginate(12);
        }
      else {
        $mobiles = mobile::paginate(12);
      }

      }
      else if($cost=="under1mil")
      {
        if($brand!="")
        {
        $mobiles = mobile::where('brand_id',$brand)->where('price','<=','1000000')->paginate(12);
        }
        else
        {
          $mobiles = mobile::where('price','<=','1000000')->paginate(12);
        }
      }
      else if($cost=="1to3mil")
      {
        if($brand!="")
        {
        $mobiles = mobile::where('brand_id',$brand)->whereBetween('price', [1000000, 3000000])->paginate(12);
        }
        else {
            $mobiles = mobile::whereBetween('price', [1000000, 3000000])->paginate(12);
        }

      }
      else if($cost=="3to7mil")
      {
        if($brand!="")
        {
        $mobiles = mobile::where('brand_id',$brand)->whereBetween('price', [3000000, 7000000])->paginate(12);
        }
        else{
                  $mobiles = mobile::whereBetween('price', [3000000, 7000000])->paginate(12);
        }
      }
      else if($cost=="7to10mil")
      {
        if($brand!="")
        {
        $mobiles = mobile::where('brand_id',$brand)->whereBetween('price', [7000000, 10000000])->paginate(12);
        }
        else {
            $mobiles = mobile::whereBetween('price', [7000000, 10000000])->paginate(12);
        }
      }
      else if($cost=="above10mil")
      {
        if($brand!="")
        {
        $mobiles = mobile::where('brand_id',$brand)->where('price','>=','10000000')->paginate(12);
        }
        else {
        $mobiles = mobile::where('price','>=','10000000')->paginate(12);
        }

      }

      //$returnHTML = view('user.mobile')->with(['mobiles'=>$mobiles,'brands'=>$brands])->render();
      return  view('layout.mobile-container-partial')->with(['mobiles'=>$mobiles])->render();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mobile  $mobile
     * @return \Illuminate\Http\Response
     */
    public function show(mobile $mobile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mobile  $mobile
     * @return \Illuminate\Http\Response
     */
    public function edit(mobile $mobile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mobile  $mobile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mobile $mobile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mobile  $mobile
     * @return \Illuminate\Http\Response
     */
    public function destroy(mobile $mobile)
    {
        //
    }


}
