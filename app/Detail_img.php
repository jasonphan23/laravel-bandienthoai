<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_img extends Model
{
   	protected $table ='detail_images';
	protected $guarded =[];

	public function products()
    {
        return $this->belongsTo('App\mobile');
    }
}
