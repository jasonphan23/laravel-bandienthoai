<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'orders' ;
    protected $fillable = [
        'user_id','status','summary', 'loai_thanh_toan'];
     public function user()
     {
     return $this->belongsTo('App\User');
    }
    public function mobiles()
    {
      return $this->belongsToMany('App\mobile','order_details');
    }
    public function Order_details()
    {
      return $this->hasMany('App\Orders_detail');
    }
}
