/*
Navicat MySQL Data Transfer

Source Server         : localhost_33062
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : shopdt

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-04 23:45:30
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('twicesangmatsanglong@gmail.com', '$2y$10$u/FgBxAukfEqFTANv7VqWOG/XRq//feQnV5JmCWJwSiSLTz/sjKgO', '2017-12-04 23:24:46');
