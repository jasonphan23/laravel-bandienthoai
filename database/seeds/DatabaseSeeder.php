<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\brand::class, 5)->create()->each(function ($b) {
        $b->posts()->save(factory(App\brand::class)->make());
    });
    }
}
