/*
Navicat MySQL Data Transfer

Source Server         : localhost_33062
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : shopdt

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-11-06 01:22:09
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `brands`
-- ----------------------------
DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of brands
-- ----------------------------
INSERT INTO `brands` VALUES ('1', 'Iphone', '0');
INSERT INTO `brands` VALUES ('2', 'SAMSUNG', '0');
INSERT INTO `brands` VALUES ('3', 'HTC', '0');
INSERT INTO `brands` VALUES ('4', 'SONY', '0');
INSERT INTO `brands` VALUES ('5', 'OPPO', '0');

-- ----------------------------
-- Table structure for `detail_images`
-- ----------------------------
DROP TABLE IF EXISTS `detail_images`;
CREATE TABLE `detail_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile_id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detail_images_mobile_id_foreign` (`mobile_id`),
  CONSTRAINT `detail_images_mobile_id_foreign` FOREIGN KEY (`mobile_id`) REFERENCES `mobiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of detail_images
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('25', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('26', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('27', '2017_10_14_063134_create_brands_table', '1');
INSERT INTO `migrations` VALUES ('28', '2017_10_14_064039_create_mobiles_table', '1');
INSERT INTO `migrations` VALUES ('29', '2017_10_14_065215_create_orders_table', '1');
INSERT INTO `migrations` VALUES ('30', '2017_10_14_070923_mobile_order', '1');
INSERT INTO `migrations` VALUES ('31', '2017_10_27_041737_create_detail_img_table', '1');
INSERT INTO `migrations` VALUES ('32', '2017_10_27_045156_create_order_details_table', '1');

-- ----------------------------
-- Table structure for `mobiles`
-- ----------------------------
DROP TABLE IF EXISTS `mobiles`;
CREATE TABLE `mobiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `screen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `platform` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `internal_memory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `battery` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_cam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondary_cam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity_left` int(11) NOT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mobiles_brand_id_foreign` (`brand_id`),
  CONSTRAINT `mobiles_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mobiles
-- ----------------------------
INSERT INTO `mobiles` VALUES ('1', 'Samsung Galaxy Note 8', '20000', '<h2><strong>Galaxy Note 8 l&agrave; niềm hy vọng vực lại d&ograve;ng Note danh tiếng của Samsung với diện mạo nam t&iacute;nh, sang trọng. Trang bị camera k&eacute;p x&oacute;a ph&ocirc;ng thời thượng, m&agrave;n h&igrave;nh v&ocirc; cực như tr&ecirc;n&nbsp;<a title=\"S8 Plus\" href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus\" target=\"_blank\" rel=\"noopener\" type=\"S8 Plus\">S8 Plus</a>, b&uacute;t Spen với nhiều t&iacute;nh năng mới v&agrave; nhiều c&ocirc;ng nghệ được Samsung ưu &aacute;i đem l&ecirc;n Note 8.</strong></h2>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"1\" src=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" alt=\"1\" data-original=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>D&ugrave; được giới thiệu l&agrave; m&agrave;n h&igrave;nh l&ecirc;n tới 6.3 inch nhưng thực sự m&aacute;y khi cầm tr&ecirc;n tay rất nhỏ gọn nhờ v&agrave;o c&ocirc;ng nghệ \"m&agrave;n h&igrave;nh v&ocirc; cực\" ti&ecirc;n tiến nhất hiện nay của Samsung.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"2\" src=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" alt=\"2\" data-original=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>C&aacute;c cạnh b&ecirc;n m&aacute;y được l&agrave;m cong đều khiến m&igrave;nh khi cầm c&oacute; cảm gi&aacute;c &ocirc;m tay rất thoải m&aacute;i v&agrave; kh&ocirc;ng hề bị cấn.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"3\" src=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" alt=\"3\" data-original=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" /></a></p>', '	Super AMOLED, 6.3\", Quad HD (2K)', '	Android 7.1', '	Exynos 8895 8 nhân 64-bit', '	6 GB', '64GB', '3300 mAh, có sạc nhanh', '8 MP', '2 Camera 12MP', '10', '2', 'samsung-note-8.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('2', 'Samsung Galaxy S8 Plus', '20000', '<h2><strong>Galaxy Note 8 l&agrave; niềm hy vọng vực lại d&ograve;ng Note danh tiếng của Samsung với diện mạo nam t&iacute;nh, sang trọng. Trang bị camera k&eacute;p x&oacute;a ph&ocirc;ng thời thượng, m&agrave;n h&igrave;nh v&ocirc; cực như tr&ecirc;n&nbsp;<a title=\"S8 Plus\" href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus\" target=\"_blank\" rel=\"noopener\" type=\"S8 Plus\">S8 Plus</a>, b&uacute;t Spen với nhiều t&iacute;nh năng mới v&agrave; nhiều c&ocirc;ng nghệ được Samsung ưu &aacute;i đem l&ecirc;n Note 8.</strong></h2>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"1\" src=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" alt=\"1\" data-original=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>D&ugrave; được giới thiệu l&agrave; m&agrave;n h&igrave;nh l&ecirc;n tới 6.3 inch nhưng thực sự m&aacute;y khi cầm tr&ecirc;n tay rất nhỏ gọn nhờ v&agrave;o c&ocirc;ng nghệ \"m&agrave;n h&igrave;nh v&ocirc; cực\" ti&ecirc;n tiến nhất hiện nay của Samsung.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"2\" src=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" alt=\"2\" data-original=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>C&aacute;c cạnh b&ecirc;n m&aacute;y được l&agrave;m cong đều khiến m&igrave;nh khi cầm c&oacute; cảm gi&aacute;c &ocirc;m tay rất thoải m&aacute;i v&agrave; kh&ocirc;ng hề bị cấn.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"3\" src=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" alt=\"3\" data-original=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" /></a></p>', 'Super AMOLED, 6.3\", Quad HD (2K)', '	Android 7.1', '	Exynos 8895 8 nhân 64-bit', '6 GB', '64GB', '3300 mAh, có sạc nhanh', '8 MP', '2 Camera 12MP', '10', '2', 's8-plus.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('3', 'Samsung Galaxy C9 Pro', '20000', '<h2><strong>Galaxy Note 8 l&agrave; niềm hy vọng vực lại d&ograve;ng Note danh tiếng của Samsung với diện mạo nam t&iacute;nh, sang trọng. Trang bị camera k&eacute;p x&oacute;a ph&ocirc;ng thời thượng, m&agrave;n h&igrave;nh v&ocirc; cực như tr&ecirc;n&nbsp;<a title=\"S8 Plus\" href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus\" target=\"_blank\" rel=\"noopener\" type=\"S8 Plus\">S8 Plus</a>, b&uacute;t Spen với nhiều t&iacute;nh năng mới v&agrave; nhiều c&ocirc;ng nghệ được Samsung ưu &aacute;i đem l&ecirc;n Note 8.</strong></h2>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"1\" src=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" alt=\"1\" data-original=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>D&ugrave; được giới thiệu l&agrave; m&agrave;n h&igrave;nh l&ecirc;n tới 6.3 inch nhưng thực sự m&aacute;y khi cầm tr&ecirc;n tay rất nhỏ gọn nhờ v&agrave;o c&ocirc;ng nghệ \"m&agrave;n h&igrave;nh v&ocirc; cực\" ti&ecirc;n tiến nhất hiện nay của Samsung.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"2\" src=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" alt=\"2\" data-original=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>C&aacute;c cạnh b&ecirc;n m&aacute;y được l&agrave;m cong đều khiến m&igrave;nh khi cầm c&oacute; cảm gi&aacute;c &ocirc;m tay rất thoải m&aacute;i v&agrave; kh&ocirc;ng hề bị cấn.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"3\" src=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" alt=\"3\" data-original=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" /></a></p>', 'Super AMOLED, 6.3\", Quad HD (2K)', '', '', '6 GB', '64GB', '3300 mAh, có sạc nhanh', '8 MP', '2 Camera 12MP', '10', '2', 'samsung-galaxy-c9-pro.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('4', 'Iphone 7 256GB', '20000', '<h2><strong>Galaxy Note 8 l&agrave; niềm hy vọng vực lại d&ograve;ng Note danh tiếng của Samsung với diện mạo nam t&iacute;nh, sang trọng. Trang bị camera k&eacute;p x&oacute;a ph&ocirc;ng thời thượng, m&agrave;n h&igrave;nh v&ocirc; cực như tr&ecirc;n&nbsp;<a title=\"S8 Plus\" href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus\" target=\"_blank\" rel=\"noopener\" type=\"S8 Plus\">S8 Plus</a>, b&uacute;t Spen với nhiều t&iacute;nh năng mới v&agrave; nhiều c&ocirc;ng nghệ được Samsung ưu &aacute;i đem l&ecirc;n Note 8.</strong></h2>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"1\" src=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" alt=\"1\" data-original=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>D&ugrave; được giới thiệu l&agrave; m&agrave;n h&igrave;nh l&ecirc;n tới 6.3 inch nhưng thực sự m&aacute;y khi cầm tr&ecirc;n tay rất nhỏ gọn nhờ v&agrave;o c&ocirc;ng nghệ \"m&agrave;n h&igrave;nh v&ocirc; cực\" ti&ecirc;n tiến nhất hiện nay của Samsung.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"2\" src=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" alt=\"2\" data-original=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>C&aacute;c cạnh b&ecirc;n m&aacute;y được l&agrave;m cong đều khiến m&igrave;nh khi cầm c&oacute; cảm gi&aacute;c &ocirc;m tay rất thoải m&aacute;i v&agrave; kh&ocirc;ng hề bị cấn.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"3\" src=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" alt=\"3\" data-original=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" /></a></p>', 'Super AMOLED, 6.3\", Quad HD (2K)', '', '', '', '', '', '8 MP', '2 Camera 12MP', '10', '1', 'iphone-7.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('5', 'Sony Xperia XZ1', '20000', 'Đặc điểm nổi bật của Sony Xperia XZs\r\n\r\nTìm hiểu thêm\r\nTìm hiểu thêm\r\nTìm hiểu thêm\r\nTìm hiểu thêm\r\nTìm hiểu thêm\r\nBộ sản phẩm chuẩn: Hộp, Sạc, Tai nghe, Sách hướng dẫn, Cáp\r\n<h2><strong>Galaxy Note 8 l&agrave; niềm hy vọng vực lại d&ograve;ng Note danh tiếng của Samsung với diện mạo nam t&iacute;nh, sang trọng. Trang bị camera k&eacute;p x&oacute;a ph&ocirc;ng thời thượng, m&agrave;n h&igrave;nh v&ocirc; cực như tr&ecirc;n&nbsp;<a title=\"S8 Plus\" href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus\" target=\"_blank\" rel=\"noopener\" type=\"S8 Plus\">S8 Plus</a>, b&uacute;t Spen với nhiều t&iacute;nh năng mới v&agrave; nhiều c&ocirc;ng nghệ được Samsung ưu &aacute;i đem l&ecirc;n Note 8.</strong></h2>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"1\" src=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" alt=\"1\" data-original=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>D&ugrave; được giới thiệu l&agrave; m&agrave;n h&igrave;nh l&ecirc;n tới 6.3 inch nhưng thực sự m&aacute;y khi cầm tr&ecirc;n tay rất nhỏ gọn nhờ v&agrave;o c&ocirc;ng nghệ \"m&agrave;n h&igrave;nh v&ocirc; cực\" ti&ecirc;n tiến nhất hiện nay của Samsung.</p>\r\n<h2><strong>Galaxy Note 8 l&agrave; niềm hy vọng vực lại d&ograve;ng Note danh tiếng của Samsung với diện mạo nam t&iacute;nh, sang trọng. Trang bị camera k&eacute;p x&oacute;a ph&ocirc;ng thời thượng, m&agrave;n h&igrave;nh v&ocirc; cực như tr&ecirc;n&nbsp;<a title=\"S8 Plus\" href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus\" target=\"_blank\" rel=\"noopener\" type=\"S8 Plus\">S8 Plus</a>, b&uacute;t Spen với nhiều t&iacute;nh năng mới v&agrave; nhiều c&ocirc;ng nghệ được Samsung ưu &aacute;i đem l&ecirc;n Note 8.</strong></h2>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"1\" src=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" alt=\"1\" data-original=\"https://cdn4.tgdd.vn/Files/2017/09/09/1020228/z764741777150_ea3f044fdfc5223e3e5b0c82a4a331f0-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>D&ugrave; được giới thiệu l&agrave; m&agrave;n h&igrave;nh l&ecirc;n tới 6.3 inch nhưng thực sự m&aacute;y khi cầm tr&ecirc;n tay rất nhỏ gọn nhờ v&agrave;o c&ocirc;ng nghệ \"m&agrave;n h&igrave;nh v&ocirc; cực\" ti&ecirc;n tiến nhất hiện nay của Samsung.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"2\" src=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" alt=\"2\" data-original=\"https://cdn1.tgdd.vn/Files/2017/09/09/1020228/z764741903200_347a0ee08cf7acf973047629cf2c29ac-_2560x1920-800-resize.jpg\" /></a></p>\r\n<p>C&aacute;c cạnh b&ecirc;n m&aacute;y được l&agrave;m cong đều khiến m&igrave;nh khi cầm c&oacute; cảm gi&aacute;c &ocirc;m tay rất thoải m&aacute;i v&agrave; kh&ocirc;ng hề bị cấn.</p>\r\n<p><a class=\"preventdefault\" href=\"https://cdn.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\"><img class=\"lazy\" title=\"3\" src=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" alt=\"3\" data-original=\"https://cdn3.tgdd.vn/Files/2017/09/09/1020228/z764741534567_c7686e1d5bdbcd1a1a365182240bd1fd-_2560x1920-800-resize.jpg\" /></a></p>', '', '', '', '', '', '', '', '', '0', '4', 'sony-xperia-xz1.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('6', 'Sony Xperia XZS', '20000', '', '', '', '', '', '', '', '', '', '0', '4', 'sony-xperia-xzs.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('7', 'HTC 11', '20000', '', '', '', '', '', '', '', '', '', '0', '3', 'htc-u11.png', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `mobiles` VALUES ('10', 'HTC U Ultra Sapphire', '20000', '', '', '', '', '', '', '', '', '', '0', '3', 'htc-u-ultra-sapphire.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('12', 'Sony Xperia XA1 Ultra', '20000', '', '', '', '', '', '', '', '', '', '0', '4', 'sony-xa1-ultra.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('14', 'OPPO F3', '20000', '', '', '', '', '', '', '', '', '', '0', '5', 'oppo-f3.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('16', 'OPPO F3 Plus', '52000', '', '', '', '', '', '', '', '', '', '0', '5', 'oppo-f3-plus.png', '0', null, null);
INSERT INTO `mobiles` VALUES ('17', 'OPPO A3', '50000', '', '', '', '', '', '', '', '', '', '0', '5', 'oppo-a3.png', '0', null, null);

-- ----------------------------
-- Table structure for `mobiles_orders`
-- ----------------------------
DROP TABLE IF EXISTS `mobiles_orders`;
CREATE TABLE `mobiles_orders` (
  `mobile_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  KEY `mobiles_orders_mobile_id_foreign` (`mobile_id`),
  KEY `mobiles_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `mobiles_orders_mobile_id_foreign` FOREIGN KEY (`mobile_id`) REFERENCES `mobiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mobiles_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mobiles_orders
-- ----------------------------

-- ----------------------------
-- Table structure for `order_details`
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `order_id` int(10) unsigned NOT NULL,
  `mobile_id` int(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `total_price` double NOT NULL,
  KEY `order_details_order_id_foreign` (`order_id`),
  KEY `order_details_mobile_id_foreign` (`mobile_id`),
  CONSTRAINT `order_details_mobile_id_foreign` FOREIGN KEY (`mobile_id`) REFERENCES `mobiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of order_details
-- ----------------------------

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `summary` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `real_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'ABCXYZ', null, 'arsenalvu@gmail.com', null, null, '1', '$2y$10$qhySjN9CU/TDPrZPszJjzebZ.26Cj4UAyl74rcT8wQxv8C0Ez1Xvm', 'tzkaCBzG6nUGEsNdYRCcMXdlZ0wm68B2LdAkSxo2m9eYMtZOD5TEmkuDI8Ra', '2017-11-05 17:18:25', '2017-11-05 17:18:25');
INSERT INTO `users` VALUES ('2', 'test', null, 'test@gmail.com', null, null, '2', '$2y$10$qhySjN9CU/TDPrZPszJjzebZ.26Cj4UAyl74rcT8wQxv8C0Ez1Xvm', 'LLE38Ec1FWl70PXe2YW3L6eWCxvcZcSXoquk6Y9dggDlsNlVSo6p1ZqxcKht', null, null);
